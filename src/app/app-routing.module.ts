import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'dublinbikes', pathMatch: 'full' },
 // { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'movies', loadChildren: './pages/movies/movies.module#MoviesPageModule' },
  { path: 'movies/:id', loadChildren: './pages/moviedetails/moviedetails.module#MoviedetailsPageModule' },
   { path: 'dublinbikes', loadChildren: './pages/dublinbikes/dublinbikes.module#DublinbikesPageModule' },
  { path: 'dublinbikes/:id', loadChildren: './pages/bikedetail/bikedetail.module#BikedetailPageModule' },
  { path: 'weather', loadChildren: './pages/weather/weather.module#WeatherPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
