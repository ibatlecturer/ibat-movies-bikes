import { Component, OnInit } from '@angular/core';
import { BikesService } from 'src/app/services/bikes.service';
import { Bikestation } from 'src/app/models/bikestation';

@Component({
  selector: 'app-dublinbikes',
  templateUrl: './dublinbikes.page.html',
  styleUrls: ['./dublinbikes.page.scss'],
})
export class DublinbikesPage implements OnInit {

  results: Bikestation[]; 
  
  constructor(private bikeService: BikesService) { }

  ngOnInit() {
    this.bikeService.getBikeStations().subscribe(jsonData => {

      this.results = jsonData;

     
    })
  }

}
