import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DublinbikesPage } from './dublinbikes.page';

describe('DublinbikesPage', () => {
  let component: DublinbikesPage;
  let fixture: ComponentFixture<DublinbikesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DublinbikesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DublinbikesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
