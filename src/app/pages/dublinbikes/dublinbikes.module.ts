import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DublinbikesPage } from './dublinbikes.page';

const routes: Routes = [
  {
    path: '',
    component: DublinbikesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DublinbikesPage]
})
export class DublinbikesPageModule {}
