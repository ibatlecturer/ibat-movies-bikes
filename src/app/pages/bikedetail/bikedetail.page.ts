import { Component, OnInit } from '@angular/core';
import { BikesService } from 'src/app/services/bikes.service';
import { ActivatedRoute } from '@angular/router';
import { Bikestation } from 'src/app/models/bikestation';

@Component({
  selector: 'app-bikedetail',
  templateUrl: './bikedetail.page.html',
  styleUrls: ['./bikedetail.page.scss'],
})
export class BikedetailPage implements OnInit {

  stationInfo: Bikestation;
  constructor(private bikeService: BikesService,
    private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    //+ means convert to integer
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.bikeService.getBikeStation(id).subscribe(result => {

      this.stationInfo = result;

    })
  }

}
