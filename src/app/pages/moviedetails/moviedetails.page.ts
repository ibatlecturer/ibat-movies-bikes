import { Component, OnInit } from '@angular/core';

import { MovieService } from 'src/app/services/movie.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-moviedetails',
  templateUrl: './moviedetails.page.html',
  styleUrls: ['./moviedetails.page.scss'],
})
export class MoviedetailsPage implements OnInit {

  movieInfo = null;
  constructor(private movieService: MovieService,
    private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.movieService.getDetails(id).subscribe(result => {

      this.movieInfo = result;

    })
  }

}
