import { Injectable } from '@angular/core';
import { Bikestation } from '../models/bikestation';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BikesService {

    url = "https://api.jcdecaux.com/vls/v1/stations";
    apiKey = "013c96fda1ac6937698c8402e42b0c31f2cc081e"
   
   
  constructor(private http:HttpClient) { }

  getBikeStations(): Observable<Bikestation[]> {
   
    const apiUrl = `${this.url}?contract=dublin&apiKey=${this.apiKey}`;
    return this.http.get<Bikestation[]>(apiUrl);
  }

    //Inform function it is returning a Bikestation 
    getBikeStation(id: string): Observable<Bikestation> {

      const apiUrl = `${this.url}/${id}?contract=dublin&apiKey=${this.apiKey}`;
      return this.http.get<Bikestation>(apiUrl);
    }

  //Inform function it is return array of Bikestation (s) as observable
  /*
  _getBikeStations(): Observable<Bikestation[]> {
    return of(

      [
        {
          number: 123,
          name: "testing",
          address: "teting addre",
          banking: false,
          status: "OPEN",
          bike_stands: 12,
          available_bike_stands: 15,
          available_bikes: 16
        },
        {
          number: 1235,
          name: "testing 12345",
          address: "teting addre",
          banking: false,
          status: "OPEN",
          bike_stands: 12,
          available_bike_stands: 15,
          available_bikes: 16


        }

      ]

    );
  }

  //Inform function it is returning a Bikestation 
  _getBikeStation(id: string): Observable<Bikestation> {

    return of(
      {
      number: 1235,
      name: "testing 12345",
      address: "teting addre",
      banking: false,
      status: "OPEN",
      bike_stands: 12,
      available_bike_stands: 15,
      available_bikes: 16


    })

  }
  */
}
