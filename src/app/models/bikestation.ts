import { Geoposition } from './geoposition';

export class Bikestation {

    number: number;
    name: string;
    address: string;
    banking: boolean;
    status: string;
    bike_stands: number;
    available_bike_stands: number;
    available_bikes: number;
    position: Geoposition
}


